<div class="profile-match-wrapper">
	<div class="profile-match-photo">
		<a href="{{$entry.url}}">
			<img src="{{$entry.photo}}" alt="{{$entry.name}}" title="{{$entry.name}} [{{$entry.profile}}]" />
		</a>
	</div>
	<div class="profile-match-break"></div>
	<div class="profile-match-name">
		<a href="{{$entry.url}}" title="{{$entry.name}}">{{$entry.name}}</a>
	<br><br>
	{{$entry.description}}
	</div>
	<div class="profile-match-end"></div>
	{{if $entry.connlnk}}
	<a class="profile-match-connect btn btn-outline-secondary connect-no-text" href="{{$entry.connlnk}}" title="{{$entry.conntxt}}"><i class="fa fa-plus connect-icon"></i></a>
	<a href="{{$entry.ignlnk}}" title="{{$entry.ignore}}" class="profile-match-connect btn btn-outline-secondary connect-no-text" onclick="return confirmDelete();" ><i class="fa fa-times drop-icons"></i></a>
	{{/if}}
</div>
